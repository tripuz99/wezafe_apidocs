---
title: Defentry API Documentation

language_tabs:
- cURL

includes:

search: true

toc_footers:
---
<!-- START_INFO -->
#General
For using our Application Programming Interface (API) you need to create client credentials for your company in API management section at your administration panel. You will get client_id and client_secret values.
##Authorization
For authorization we use OAuth2.0 "client_credentials" grant type. Thus, after creating your client's credentials, you need to obtain an access token to access our API endpoints. This access token should be added to the "Authorization" header for each request to our API. So, after creating your client credentials to get access to our API endpoints you need to get access token. This access token should be added to "Authorization" header for each request to our api.
##Tokens
###Access tokens
This token should be used to get access to our API endpoints. You can generate it using your client credentials by sending POST request to "/api/token" endpoint. Access token expires in 6 months after creating.
##Headers
For each request to our API you should provide in general 2 required HTTP headers:

1."Accept: application/json";

2."Authorization:  Bearer {access_token}";

For methods PUT and POST you also should provide next additional header "Content-Type": "application/json". See endpoints requests examples.
##Get Countries
>Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/countries"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "id": 73,
            "name": "Finland",
            "code": "FI"
        },
        {
            "id": 211,
            "name": "Sweden",
            "code": "SE"
        }
    ]
}
```
Use this endpoint to get Countries
###HTTP Request:
`POST https://api.wezafe.se/api/v1/countries{user_id}`
##Get Plans
>Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/plan/1"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
```
>Example response: 200 OK

```json
{
    "data": {
        "id": 1,
        "title": "WeZafe Free",
        "short_title": "WeZafe Free",
        "description": "WeZafe Free plan with all free features",
        "type": "SINGLE",
        "interval": null,
        "plan_type": "FREE",
        "total_plan_duration_in_month": null,
        "currency": null,
        "full_payment_amount": null,
        "country": null,
        "trial_duration_in_days": null,
        "extended_description": "Unlimited searches at Hackad.se (to check it your email accounts have been hacked)<br>24/7 realtime monitoring of two email addresses<br>Newsletters, free access to our knowledge base and all guides",
        "url": "user/payments/subscription/single",
        "is_cancellable": 0,
        "price_formatted": "0  / yearly"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/plan/1"
    }
}
```
Use this endpoint to get all plans/plan by plan_id
###HTTP Request:
`GET https://api.wezafe.se/api/v1/plan/{plan_id}`

#Account
##Register new account
> Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/auth/register"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-d '{"email":"{test2@test.com}"},{"password":"{password}"},{"first_name":"John"},{"last_name":"Bush"},
{"password_confirmation":"password"},{"country_code":"SE"}'
```
>Example response: 201 CREATED

```json
{
  "data": {
      "account": {
          "id": "89",
          "first_name": "vitaliy",
          "last_name": "apitest",
          "email": "tripuz2@gmail.com",
          "ssn": null,
          "image": null,
          "country": "SE",
          "country_full": "Sweden",
          "city": null,
          "street": null,
          "zipcode": null,
          "payment_city": null,
          "payment_street": null,
          "payment_zipcode": null,
          "phone": null,
          "type": "SINGLE",
          "parent_id": null,
          "has_used_trial": null,
          "is_phone_confirmed": false,
          "is_email_confirmed": false,
          "is_ssn_confirmed": false,
          "is_password_set": true,
          "shield_status": "GREEN",
          "new_alarms_count": 0,
          "total_alarms_count": 0,
          "created_at": "2018-11-21 11:23:16",
          "updated_at": "2018-11-21 11:23:16"
      },
      "subscription": {
          "status": "ACTIVE",
          "start_date": "2018-11-21",
          "end_date": null,
          "cancellation_date": null,
          "next_payment_date": null,
          "trial_end_date": null,
          "plan": {
              "id": 1,
              "title": "WeZafe Free",
              "short_title": "WeZafe Free",
              "description": "WeZafe Free plan with all free features",
              "type": "SINGLE",
              "interval": null,
              "plan_type": "FREE",
              "total_plan_duration_in_month": null,
              "currency": null,
              "full_payment_amount": null,
              "country": null,
              "trial_duration_in_days": null,
              "extended_description": "Unlimited searches at Hackad.se (to check it your email accounts have been hacked)<br>24/7 realtime monitoring of two email addresses<br>Newsletters, free access to our knowledge base and all guides",
              "url": "user/payments/subscription/single",
              "is_cancellable": 0,
              "price_formatted": "0  / yearly"
          },
          "payment_details": {
              "provider_name": null,
              "provider_payment_method": null,
              "payment_account": null,
              "available_payment_accounts": {
                  "has_stripe_account": false,
                  "has_arvato_account": false
              }
          }
      },
      "company": null,
      "personal_protection": {
          "credit_score": null,
          "credit_score_text": null,
          "active": false
      }
  },
  "links": {
      "self": "http://api.stage.wezafe.se/api/v1/auth/register"
  },
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0NWIwZGY0Zjg2ZDE0M2E2OTJiYzkzZmI0NDQxNzUyMDQ5MDkzYzExY2NkMjk1Zjc5MWRlMDRkODBkOTNmNzZkMGM1YWQ5ZjAzMTc3ZmJjIn0.eyJhdWQiOiI5IiwianRpIjoiMDQ1YjBkZjRmODZkMTQzYTY5MmJjOTNmYjQ0NDE3NTIwNDkwOTNjMTFjY2QyOTVmNzkxZGUwNGQ4MGQ5M2Y3NmQwYzVhZDlmMDMxNzdmYmMiLCJpYXQiOjE1NDI3OTkzOTksIm5iZiI6MTU0Mjc5OTM5OSwiZXhwIjoxNTc0MzM1Mzk5LCJzdWIiOiI4OSIsInNjb3BlcyI6W119.jcNqLJrRXa_9IPdN-1N0ZTIPUOsog5ynHq22gp9CYzNk9U57oY0Bz2dlWnTFCTacExiL3Uw35DBlICgPWNuJmEXlZkDm-3WF38ccIor0aVSbmPdbNbws_5pYbYobImJuvjaoygEVFtxLWmrnadPc9n0ooI945hwXANu8s8rw5extetDoqu9P2Ho0EoRiiM4qVId1FAnH8LykoEpmXFjSkmpXvC-QD4iJ3vGDHmKyha65ldciM_cSWFP6yrMrKxqzyqd2-kHJcKXFIpbuf_JNMXa7ZFDFDBhrm3txJ2WO8QfYvTg5_8ZG7Fmuim4hSgYLH4JYrIO9JuUgZ0jpCAEBNrOq74BP85maUq59-llZKn9OkaFrKPiUwcTrIBmxif6arwDsHarlaibp8amczbhiQkb7Ooi5OsMrzyVGKiMILPr-k5gdtVlD6LqENWUh_L_y7BQqG2fDU71Lt-NjPsG15KpYHPkSy9hl5R-PIJ82IbIGlrz39aknWQBbbhJ-oGe3jtZ3JUIjnHWBy-0vBzLSOr_aEeHcXdifgLu9V01EaoT9hBCGpxEiR4h4fEJHa5daF8mH5w3DcrY8Vyqlct1TFXUApMAuk-G2yDIUdFEZeqFUBXUwy8fH7kWWqtk-mVVzkx5hJO5GU-9G7RafOkZDyxqir4Allbom-y2Vd5GtH10"
}
```
Use this endpoint to register user on WeZafe.
###HTTP Request:
`POST https://api.wezafe.se/api/v1/auth/register{user_id}`

###Parameters

Parameter | type | Required | Example
-------------- | -------------- | --------------
email | string | yes | test@test.com
password | string | yes | wezafesuperpassword
password_confirmation | string | yes | wezafesuperpassword
first_name | string | yes | Steve
last_name | string | yes | Jobs
country_code | string | yes | SE/FI

##Resend welcome letter
>Example request

```json
curl -X GET "http://api.stage.wezafe.se/api/v1/user/email/resend-welcome"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
```
>Example response: 201 CREATED

Use this endpoint to resend welcome letter with email-confirmation link
###HTTP Request:
`GET http://api.stage.wezafe.se/api/v1/user/email/resend-welcome`

##Login to existing account

> Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/auth/login"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-d '{"email":"{test2@test.com}"},{"password":"{password}"}'
```

>Example response: 200 OK

```json
{
  "data": {
      "account": {
          "id": "89",
          "first_name": "vitaliy",
          "last_name": "apitest",
          "email": "tripuz2@gmail.com",
          "ssn": null,
          "image": null,
          "country": "SE",
          "country_full": "Sweden",
          "city": null,
          "street": null,
          "zipcode": null,
          "payment_city": null,
          "payment_street": null,
          "payment_zipcode": null,
          "phone": null,
          "type": "SINGLE",
          "parent_id": null,
          "has_used_trial": null,
          "is_phone_confirmed": false,
          "is_email_confirmed": false,
          "is_ssn_confirmed": false,
          "is_password_set": true,
          "shield_status": "GREEN",
          "new_alarms_count": 0,
          "total_alarms_count": 0,
          "created_at": "2018-11-21 11:23:16",
          "updated_at": "2018-11-21 11:23:16"
      },
      "subscription": {
          "status": "ACTIVE",
          "start_date": "2018-11-21",
          "end_date": null,
          "cancellation_date": null,
          "next_payment_date": null,
          "trial_end_date": null,
          "plan": {
              "id": 1,
              "title": "WeZafe Free",
              "short_title": "WeZafe Free",
              "description": "WeZafe Free plan with all free features",
              "type": "SINGLE",
              "interval": null,
              "plan_type": "FREE",
              "total_plan_duration_in_month": null,
              "currency": null,
              "full_payment_amount": null,
              "country": null,
              "trial_duration_in_days": null,
              "extended_description": "Unlimited searches at Hackad.se (to check it your email accounts have been hacked)<br>24/7 realtime monitoring of two email addresses<br>Newsletters, free access to our knowledge base and all guides",
              "url": "user/payments/subscription/single",
              "is_cancellable": 0,
              "price_formatted": "0  / yearly"
          },
          "payment_details": {
              "provider_name": null,
              "provider_payment_method": null,
              "payment_account": null,
              "available_payment_accounts": {
                  "has_stripe_account": false,
                  "has_arvato_account": false
              }
          }
      },
      "company": null,
      "personal_protection": {
          "credit_score": null,
          "credit_score_text": null,
          "active": false
      }
  },
  "links": {
      "self": "http://api.stage.wezafe.se/api/v1/auth/login"
  },
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA0NWIwZGY0Zjg2ZDE0M2E2OTJiYzkzZmI0NDQxNzUyMDQ5MDkzYzExY2NkMjk1Zjc5MWRlMDRkODBkOTNmNzZkMGM1YWQ5ZjAzMTc3ZmJjIn0.eyJhdWQiOiI5IiwianRpIjoiMDQ1YjBkZjRmODZkMTQzYTY5MmJjOTNmYjQ0NDE3NTIwNDkwOTNjMTFjY2QyOTVmNzkxZGUwNGQ4MGQ5M2Y3NmQwYzVhZDlmMDMxNzdmYmMiLCJpYXQiOjE1NDI3OTkzOTksIm5iZiI6MTU0Mjc5OTM5OSwiZXhwIjoxNTc0MzM1Mzk5LCJzdWIiOiI4OSIsInNjb3BlcyI6W119.jcNqLJrRXa_9IPdN-1N0ZTIPUOsog5ynHq22gp9CYzNk9U57oY0Bz2dlWnTFCTacExiL3Uw35DBlICgPWNuJmEXlZkDm-3WF38ccIor0aVSbmPdbNbws_5pYbYobImJuvjaoygEVFtxLWmrnadPc9n0ooI945hwXANu8s8rw5extetDoqu9P2Ho0EoRiiM4qVId1FAnH8LykoEpmXFjSkmpXvC-QD4iJ3vGDHmKyha65ldciM_cSWFP6yrMrKxqzyqd2-kHJcKXFIpbuf_JNMXa7ZFDFDBhrm3txJ2WO8QfYvTg5_8ZG7Fmuim4hSgYLH4JYrIO9JuUgZ0jpCAEBNrOq74BP85maUq59-llZKn9OkaFrKPiUwcTrIBmxif6arwDsHarlaibp8amczbhiQkb7Ooi5OsMrzyVGKiMILPr-k5gdtVlD6LqENWUh_L_y7BQqG2fDU71Lt-NjPsG15KpYHPkSy9hl5R-PIJ82IbIGlrz39aknWQBbbhJ-oGe3jtZ3JUIjnHWBy-0vBzLSOr_aEeHcXdifgLu9V01EaoT9hBCGpxEiR4h4fEJHa5daF8mH5w3DcrY8Vyqlct1TFXUApMAuk-G2yDIUdFEZeqFUBXUwy8fH7kWWqtk-mVVzkx5hJO5GU-9G7RafOkZDyxqir4Allbom-y2Vd5GtH10"
}
```
Use this endpoint for login to existing account and receive from response access_token
###HTTP Request:
`POST https://api.wezafe.se/api/v1/auth/login`

###Parameters

Parameter | type | Required | Example
-------------- | -------------- | --------------
email | string | yes | test@test.com
password | string | yes | wezafesuperpassword

##Get user's profile
> Example request:

```json
curl -X GET "https://api.wezafe.se/api/v1/user/profile"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
```
>Example response: 200 OK

``` json
{
    "data": {
        "account": {
            "id": "89",
            "first_name": "vitaliy",
            "last_name": "apitest",
            "email": "tripuz2@gmail.com",
            "ssn": null,
            "image": null,
            "country": "SE",
            "country_full": "Sweden",
            "city": null,
            "street": null,
            "zipcode": null,
            "payment_city": null,
            "payment_street": null,
            "payment_zipcode": null,
            "phone": null,
            "type": "SINGLE",
            "parent_id": null,
            "has_used_trial": 0,
            "is_phone_confirmed": false,
            "is_email_confirmed": false,
            "is_ssn_confirmed": false,
            "is_password_set": true,
            "shield_status": "GREEN",
            "new_alarms_count": 0,
            "total_alarms_count": 0,
            "created_at": "2018-11-21 11:23:16",
            "updated_at": "2018-11-21 11:38:39"
        },
        "subscription": {
            "status": "ACTIVE",
            "start_date": "2018-11-21",
            "end_date": null,
            "cancellation_date": null,
            "next_payment_date": null,
            "trial_end_date": null,
            "plan": {
                "id": 1,
                "title": "WeZafe Free",
                "short_title": "WeZafe Free",
                "description": "WeZafe Free plan with all free features",
                "type": "SINGLE",
                "interval": null,
                "plan_type": "FREE",
                "total_plan_duration_in_month": null,
                "currency": null,
                "full_payment_amount": null,
                "country": null,
                "trial_duration_in_days": null,
                "extended_description": "Unlimited searches at Hackad.se (to check it your email accounts have been hacked)<br>24/7 realtime monitoring of two email addresses<br>Newsletters, free access to our knowledge base and all guides",
                "url": "user/payments/subscription/single",
                "is_cancellable": 0,
                "price_formatted": "0  / yearly"
            },
            "payment_details": {
                "provider_name": null,
                "provider_payment_method": null,
                "payment_account": null,
                "available_payment_accounts": {
                    "has_stripe_account": false,
                    "has_arvato_account": false
                }
            }
        },
        "company": null,
        "personal_protection": {
            "credit_score": null,
            "credit_score_text": null,
            "active": false
        }
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/profile"
    }
}
```
Use this endpoint to get information about some account
###HTTP Request:
`GET https://api.wezafe.se/api/v1/user/profile`

##Update user's profile data
> Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/user/profile"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
-d '{"first_name":"changed first_name"},{"last_name":"changed last_name"},{"city":"Stockholm"},
{"zipcode":"1234567"},{"country":"SE/FI"}'
```

>Example response:

```json
{
    "data": {
        "account": {
            "id": "89",
            "first_name": "test",
            "last_name": "test",
            "email": "tripuz2@gmail.com",
            "ssn": null,
            "image": null,
            "country": "SE",
            "country_full": "Sweden",
            "city": "asdlasl City",
            "street": null,
            "zipcode": "123456",
            "payment_city": null,
            "payment_street": null,
            "payment_zipcode": null,
            "phone": null,
            "type": "SINGLE",
            "parent_id": null,
            "has_used_trial": 0,
            "is_phone_confirmed": false,
            "is_email_confirmed": false,
            "is_ssn_confirmed": false,
            "is_password_set": true,
            "shield_status": "GREEN",
            "new_alarms_count": 0,
            "total_alarms_count": 0,
            "created_at": "2018-11-21 11:23:16",
            "updated_at": "2018-11-21 11:52:01"
        },
        "subscription": {
            "status": "ACTIVE",
            "start_date": "2018-11-21",
            "end_date": null,
            "cancellation_date": null,
            "next_payment_date": null,
            "trial_end_date": null,
            "plan": {
                "id": 1,
                "title": "WeZafe Free",
                "short_title": "WeZafe Free",
                "description": "WeZafe Free plan with all free features",
                "type": "SINGLE",
                "interval": null,
                "plan_type": "FREE",
                "total_plan_duration_in_month": null,
                "currency": null,
                "full_payment_amount": null,
                "country": null,
                "trial_duration_in_days": null,
                "extended_description": "Unlimited searches at Hackad.se (to check it your email accounts have been hacked)<br>24/7 realtime monitoring of two email addresses<br>Newsletters, free access to our knowledge base and all guides",
                "url": "user/payments/subscription/single",
                "is_cancellable": 0,
                "price_formatted": "0  / yearly"
            },
            "payment_details": {
                "provider_name": null,
                "provider_payment_method": null,
                "payment_account": null,
                "available_payment_accounts": {
                    "has_stripe_account": false,
                    "has_arvato_account": false
                }
            }
        },
        "company": null,
        "personal_protection": {
            "credit_score": null,
            "credit_score_text": null,
            "active": false
        }
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/profile"
    }
}
```
Use this endpoint to make changes in user's profile
###HTTP Request:
`POST https://api.wezafe.se/api/v1/user/profile`

###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
city | string | no | test@test.com
zipcode | int | no | 123456
first_name | string | yes | Steve
last_name | string | yes | Jobs
country | string | no | SE/FI

##Change user's password
> Example request:

```json
curl -X POST "https://api.wezafe.se/api/v1/user/change-password"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
-d '{"current_password":"password"},{"new_password":"111111"},{"new_password_confirmation":"111111"}'
```
>Example response: 204 NO Content

Use this endpoint to change user's password
###HTTP Request:
`POST https://api.wezafe.se/api/v1/user/change-password`

###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
current_password | string | yes | password
new_password | string | yes | 111111
new_password_confirmation | string | yes | 111111
##Reset user's password (get email link)
> Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/password/reset-code/email"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-d '{"email":"email@emai.com"}'
```
>Example response: 204 NO Content

Use this endpoint to get recover link to user's email
###HTTP Request:
`POST https://api.wezafe.se/api/v1/user/change-password`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
email | string | yes | email@email.com
##Reset user's password
> Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/password/reset"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-d '{"new_password":"111111"},{"new_password_confirmation":"111111"},{"code":"57509fa00cc313a3a1a11356428196a0"}'
```
>Example response: 204 NO Content

Use this endpoint to reset user's password. {code} can be used only one time
###HTTP Request:
`POST http://api.stage.wezafe.se/api/v1/password/reset`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
new_password | string | yes | 111111
new_password_confirmation | string | yes | 111111
code | string | yes | This code user get from link in email

#News&guides
##Get News
```json
curl -X GET "https://api.wezafe.se/api/v1/news"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "id": 1,
            "date": "2018-04-22 21:08:30",
            "link": null,
            "title": "Slingshot – databrottslighet på avancerad nivå!",
            "content": "content"
        },
        {
            "id": 2,
            "date": "2018-03-13 22:31:33",
            "link": null,
            "title": "Är nättroll ett hot mot demokratin?",
            "content": "content"
        },
        {
            "id": 4,
            "date": "2018-02-14 11:52:08",
            "link": null,
            "title": "Bitcoin kan bli ett stort miljö- och säkerhetsproblem",
            "content": "content"
        },
        {
            "id": 5,
            "date": "2018-02-07 22:24:09",
            "link": null,
            "title": "Vad lärde vi oss om cybersäkerhet 2017?",
            "content": "content"
        },
        {
            "id": 6,
            "date": "2018-01-30 23:32:05",
            "link": null,
            "title": "Vad är NIST och hur kan beteendebiometri ge säkrare inloggningar?",
            "content": "content"
        },
        {
            "id": 7,
            "date": "2018-01-22 23:25:06",
            "link": null,
            "title": "Mjukvaror förbättrar sig själva genom maskininlärning",
            "content": "content"
        },
        {
            "id": 8,
            "date": "2017-12-01 03:59:52",
            "link": null,
            "title": "En säkerhetsmiss som är svår för Apple att försvara",
            "content": "content"
        },
        {
            "id": 10,
            "date": "2017-11-20 21:34:08",
            "link": null,
            "title": "Vault 8 avslöjar källkoder och Hiveprojektet",
            "content": "content"
        },
        {
            "id": 11,
            "date": "2017-11-02 05:37:38",
            "link": null,
            "title": "Digitala utpressare sprider virus som kapar datorer över hela världen",
            "content": "content"
        },
        {
            "id": 13,
            "date": "2017-10-25 16:00:27",
            "link": null,
            "title": "SVRs stora skam: En halv miljon lösenord läckta online",
            "content": "content"
        }
    ],
    "links": {
        "first": "https://api.wezafe.se/api/v1/news?page=1",
        "last": "https://api.wezafe.se/api/v1/news?page=3",
        "prev": null,
        "next": "https://api.wezafe.se/api/v1/news?page=2",
        "self": "https://api.wezafe.se/api/v1/news"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 3,
        "path": "https://api.wezafe.se/api/v1/news",
        "per_page": 10,
        "to": 10,
        "total": 26
    }
}
```
Use this endpoint to get all news/news by ID
###HTTP Request:
`GET https://api.wezafe.se/api/v1/news/{news_id}`
###Query Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
id | int | no | 1069
##Get Guides
```json
curl -X GET "https://api.wezafe.se/api/v1/guides"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "id": 360005002431,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360005002431-Kreditscore-vad-%C3%A4r-det-.json",
            "created_at": "2018-06-20 10:19:53",
            "name": "Kreditscore, vad är det?",
            "title": "Kreditscore, vad är det?",
            "body": "<p>För att förutsäga framtida händelser så använder man historisk data av privatpersoner som sedan värderas. På så sätt försöker man mäta risken och sannolikheten för att en kund inte ska erhålla ett skuldsaldo, så kallade e-mål hos kronofogden inom 12 månader. Resultatet av mätningen sammanställs i form av poäng och ju högre poäng desto bättre betalningsförmåga. På så sätt vet kreditgivare och långivare vem dem skall säga ja eller nej till och kan vara säker på att deras kreditgivning är lönsam.</p>\n<p> </p>\n<p>Som Wezafe kund får du en snabb överblick över ditt kreditbetyg och dina lån/krediter samt realtids alarm vid förändring. Du får även tips och råd om hur du kan förbättra ditt kreditbetyg och informationen som finns kan du använda exempelvis vid förhandlingar med din bank.</p>",
            "locale": "sv"
        },
        {
            "id": 360004555431,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004555431-Logga-in-p%C3%A5-hemsidan-med-bank-ID-.json",
            "created_at": "2018-06-05 15:29:30",
            "name": "Logga in på hemsidan med bank-ID ",
            "title": "Logga in på hemsidan med bank-ID ",
            "body": "<p>Mina sidor på vår hemsida <a href=\"https://app.wezafe.com/\">https://app.wezafe.com/</a> är naven i vår bevakningstjänst, som ger dig både översikt över dina tjänster hos oss och kontroll över de bevakningar du har registrerat. Där kan du ta del av larm, aktivera fler bevakningstjänster, uppgradera din prenumeration, registrera familjemedlemmar, ändra dina personuppgifter och mycket mer.</p>\n<p>Följ nedanstående steg för att logga in med Bank-ID.</p>\n<ol>\n<li>Gå in på <a href=\"https://app.wezafe.com/\">https://app.wezafe.com/</a>\n</li>\n<li>Välj att logga in med mobilt BankID eller BankID.</li>\n<li>Knappa in ditt personnummer med 12 siffror utan bindestreck (19XXXXXXXXXX) och klicka sedan på logga in.</li>\n<li>Öppna ditt BankID för att legitimera dig.</li>\n<li>Efter BankID signaturen går du till Mitt WeZafe fönster i enheten du använder för att slutföra inloggningen.</li>\n</ol>\n<p> </p>",
            "locale": "sv"
        },
        {
            "id": 360004089632,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004089632-Vanliga-l%C3%B6senord-och-misstag-att-undvika-.json",
            "created_at": "2018-05-22 13:29:28",
            "name": "Vanliga lösenord och misstag att undvika?",
            "title": "Vanliga lösenord och misstag att undvika?",
            "body": "<p>Kriminella som sysslar med lösenordsstöld använder ofta sofistikerade metoder och programvaror för att dechiffrera lösenord.</p>\n<p>Undvik alltid lösenord som innehåller något av följande</p>\n<ul>\n<li>Personlig information. Exempelvis ditt personnummer, namn på släktingar, husdjur, telefonnumer, adress etc.</li>\n<li>Ord som finns med i ordlistor.</li>\n<li>Upprepade tecken eller tecken i en logisk följd (123455, 1111111, abcdef, asdfg, qwerty)</li>\n</ul>",
            "locale": "sv"
        },
        {
            "id": 360004131811,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004131811-Hur-ska-jag-g%C3%B6ra-f%C3%B6r-att-komma-ih%C3%A5g-ett-starkt-l%C3%B6senord-.json",
            "created_at": "2018-05-22 13:28:58",
            "name": "Hur ska jag göra för att komma ihåg ett starkt lösenord?",
            "title": "Hur ska jag göra för att komma ihåg ett starkt lösenord?",
            "body": "<p>Ett starkt lösenord är inte till mycket nytta om du inte kan komma ihåg det. Det finns många sätt att skapa ett säkert lösenord, men här följer ett exempel på hur du kan skapa ett säkert lösenord som du kan komma ihåg.</p>\n<ul>\n<li>Börja med en mening som du har lätt för att komma ihåg. Exempel: Komplexa lösenord är säkra</li>\n<li>Ta bort mellanrummen mellan orden och gör första bokstaven på varje ord till stor bokstav. Exempel: KomplexaLösenordÄrSäkra</li>\n<li>Förkorta ord eller felstava ord med mening. Exempel: KompleksaLösenodESäkra.</li>\n<li>Lägg till eller byt ut bokstäver mot siffror och specialtecken. Exempel: Kompl3xaLösenodE$äkra.</li>\n<li>Du kan göra versioner av ditt lösenord för olika webbplatser och tjänster. Exempel: Kompl3xaLösenodE$äkra.H0tmejl, Kompl3xaLösenodE$äkra.Fäjsb00k</li>\n<li>Vissa webbplatser och tjänster har en maxgräns för hur många tecken lösenordet tillåter. Försök skapa ett lösenord så nära maxgränsen som möjligt</li>\n</ul>",
            "locale": "sv"
        },
        {
            "id": 360004131771,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004131771-Hur-skapar-jag-ett-starkt-och-s%C3%A4kert-l%C3%B6senord-.json",
            "created_at": "2018-05-22 13:28:22",
            "name": "Hur skapar jag ett starkt och säkert lösenord?",
            "title": "Hur skapar jag ett starkt och säkert lösenord?",
            "body": "<p>Med ett starkt och säkert lösenord gör du det svårt för någon obehörig att lista ut vad du använder som lösenord. Ett idealiskt lösenord är både långt och komplext. Det bör innehålla siffror, bokstäver, skiljetecken och specialtecken</p>\n<ul>\n<li>Ett starkt lösenord består av siffror, specialtecken (t ex !@$?), stora och små bokstäver och är minst 12 tecken långt.</li>\n<li>Välj aldrig ett lösenord som kan kopplas till dig på något sätt, exempelvis bilens registreringsskylt eller personnummer</li>\n<li>Använd inte samma lösenord på flera webbplatser. Om någon obehörig kommer åt ditt användarnamn och lösenord i en databas med låg säkerhet, kan de då inte använda lösenordet för att komma åt din information på andra, säkrare webbplatser.</li>\n<li>Byt ditt lösenord ofta. Ställ in en påminnelse till dig själv att byta lösenord för exempelvis din e-post var tredje månad. Just din e-post är viktig att hålla säker, eftersom det oftast är den du kan använda för att återställa lösenordet på ett kapat konto.</li>\n<li>Ju större variation av tecken desto bättre. Blanda därför stora och små bokstäver, siffror, skiljetecken och specialtecken</li>\n<li>Använd hela tangentbordet. Använd inte bara de bokstäver och tecken som är vanligast.</li>\n<li>Använd inte vanliga ord som \"sommar\" eller kombinationer, exempelvis qwerty (tecken i en följd på tangentbordet) eller 12345.</li>\n<li>Byt lösenord om du tror att någon annan känner till det eller om du har haft det länge.</li>\n<li>Se ditt lösenord som din egen hemlighet – dela inte ut det till någon!</li>\n<li>Memorera dina lösenord. Ett tips är att sätta in lösenordet i ett sammanhang, eller i en ramsa.</li>\n<li>Om du sparar dina lösenord i webbläsaren kan andra som har tillgång till datorn logga in på dina sidor.</li>\n</ul>",
            "locale": "sv"
        },
        {
            "id": 360004131731,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004131731-Det-st%C3%A5r-i-mitt-larm-att-mitt-l%C3%B6senord-kan-ha-%C3%A4ventyrats-Hur-kunde-detta-h%C3%A4nda-.json",
            "created_at": "2018-05-22 13:27:47",
            "name": "Det står i mitt larm att mitt lösenord kan ha äventyrats. Hur kunde detta hända?",
            "title": "Det står i mitt larm att mitt lösenord kan ha äventyrats. Hur kunde detta hända?",
            "body": "<p>Hackare arbetar hårt för att hacka sig på olika webbplatser och stjäla de uppgifter som finns lagrade där. Speciellt attraktivt är information om användarna. De bokför ofta denna information på nätet för så kallat \"lulz\" eller ryktbarhet. Vi samlar in sådana data som hackare släpper till allmänheten, data som ofta innehåller e-postadresser och lösenord. I det här fallet har en av de webbplatser som du har skapat ett konto hos förmodligen äventyrats, och dina uppgifter publiceras på nätet.<br> <br>Olika webbplatser lagrar användaruppgifter som lösenord på olika sätt. Vissa äldre webbplatser lagrade till och med sådana uppgifter i klartext, vilket gjorde det enkelt för en hackare som tagit sig förbi deras yttre skydd att plocka ut sådana uppgifter. Detta förekommer dock i praktiken inte på moderna webbplatser. Där krypteras användaruppgifter på olika sätt. Det finns dock olika former av krypteringar. Vissa krypteringar går att knäcka för den som har tillräcklig datakraft, och då går det att återskapa ett lösenord. Andra, modernare krypteringar går i dagsläget inte att knäcka ens om du har tillgång till riktigt kraftfulla datorer. Tyvärr har inte alla webbplatser implementerat denna högre nivå av säkerhet. Det är ofta en kostnadsfråga eller på annat sätt en fråga om otillräckliga resurser.</p>",
            "locale": "sv"
        },
        {
            "id": 360004131711,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004131711-Var-har-mina-larm-kommit-fr%C3%A5n-.json",
            "created_at": "2018-05-22 13:27:14",
            "name": "Var har mina larm kommit från?",
            "title": "Var har mina larm kommit från?",
            "body": "<p>Vi har fyra huvudsakliga informationskällor som vi baserar våra larm på:</p>\n<ol>\n<li>Avläsningsinfrastruktur. Under de senaste åren har vi byggt en avancerad infrastruktur för dataskörd som söker av webben, så kallad crawling, dygnet runt, året runt. Denna infrastruktur samlar potentiellt känsliga uppgifter som kan ha publicerats som en följd av en säkerhetsöverträdelse. Majoriteten av våra uppgifter kommer från den här sortens skördare.</li>\n<li>Dataläckor. Vi har ett stort nätverk av säkerhetsforskare runt om i världen som förser oss med tips på nya dataläckor som de upptäcker i sitt dagliga arbete. Ganska ofta får vi också kännedom om läckor från tredjepartsobservatörer, och ibland även hackare.</li>\n<li>Säkerhetsresearchteam. Varje vecka så lägger de säkerhetsforskare som vi har knutna till oss en betydande mängd tid på att leta eventuella nya säkerhetsöverträdelser. När sådana upptäcks gör de allt de kan för att samla de resulterande datadumparna.</li>\n<li>Vi har partnerskap med andra företag som tillhandahåller viktig person-, bank-, myndighets- och kreditupplysningsdata. När vi får ett larm fråm dem så larmar vi i realtid våra kunder.</li>\n</ol>",
            "locale": "sv"
        },
        {
            "id": 360004089512,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004089512-Jag-fick-ett-larm-som-s%C3%A4ger-mitt-konto-har-%C3%A4ventyrats-Vad-g%C3%B6r-jag-nu-.json",
            "created_at": "2018-05-22 13:25:55",
            "name": "Jag fick ett larm som säger mitt konto har äventyrats. Vad gör jag nu?",
            "title": "Jag fick ett larm som säger mitt konto har äventyrats. Vad gör jag nu?",
            "body": "<p>Om du har fått ett e-postmeddelande eller sms från vår bevakningstjänst, är det mycket troligt att ett eller flera av dina konton har äventyrats. Vi meddelar bara våra abonnenter av när deras e-postadress har hittats i en dataläckage med tillhörande lösenord.</p>\n<h2>Vad ska jag göra?</h2>\n<p>Det viktigaste första steget är att omedelbart ändra lösenordet för dina äventyrade konton. Därefter bör du ändra lösenord för alla dina andra konton, speciellt om du har använt samma lösenord på flera tjänster, och säkerställa att det inte har förekommit några otillåtna ändringar på något av dina konton.</p>",
            "locale": "sv"
        },
        {
            "id": 360004131451,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004131451-%C3%84r-WeZafe-en-webbplats-f%C3%B6r-n%C3%A4tfiske-Hur-kan-jag-lita-p%C3%A5-er-.json",
            "created_at": "2018-05-22 13:25:08",
            "name": "Är WeZafe en webbplats för nätfiske? Hur kan jag lita på er?",
            "title": "Är WeZafe en webbplats för nätfiske? Hur kan jag lita på er?",
            "body": "<p>Detta är absolut inte fråga om nätfiske. Vår databas (som idag är en av världens största) startades som ett projekt av säkerhetsforskare som ville ta itu med det aktuella läget för webbsäkerhet när det gäller dataintrång. Bakgrunden är att dataintrång tyvärr har blivit en daglig företeelse som drabbar allt fler privatpersoner och även företag och organisationer. Med över tio års erfarenhet gör vår partner InfoArmor livet enklare, säkrare och lyckligare.</p>",
            "locale": "sv"
        },
        {
            "id": 360004089492,
            "url": "https://wezafe.zendesk.com/api/v2/help_center/sv/articles/360004089492-Vad-%C3%A4r-Dark-web-.json",
            "created_at": "2018-05-22 13:24:30",
            "name": "Vad är Dark web?",
            "title": "Vad är Dark web?",
            "body": "<ul>\n<li>The Dark Web är där de flesta av Internets olagliga verksamhet finns. Där kan man köpa personuppgifter, användarnamn, kreditkort, förfalskade pass och körkort, hackerverktyg och en massa andra saker.</li>\n<li>Studier visar att Google faktiskt bara katalogiserar och söker av 16% av ytan på webben. Det innebär att Internet är långt större än du antagligen tror.</li>\n<li>Vi söker av internets svarta marknadsplatser (Dark Web) och andra anonyma delar av internet för att kunna bevaka om dina personuppgifter förekommer i riskfyllda sammanhang.</li>\n<li>Vi söker igenom över en miljard datapunkter varje dag (!) och letar efter potentiella hot mot dig och din identitet.</li>\n<li>Vi fokuserar på datakällor där vi vet att e-brottslighet sker samt bevakar dina data och personuppgifter för att få högsta möjliga kvalitet i vår bevakning och våra larm.</li>\n<li>Vi utnyttjar både automatisk teknik och manuella källor med våra säkerhetsanalytiker för att ge ett sammanhang till resultaten. Vår målsättning är att ge dig snabba larm så snart det förekommer misstänkt aktivitet eller intrång, samtidigt som vi vill minimera risken för falska alarm.</li>\n</ul>",
            "locale": "sv"
        }
    ],
    "links": {
        "first": "https://api.wezafe.se/api/v1/guides?page=1",
        "last": "https://api.wezafe.se/api/v1/guides?page=7",
        "prev": null,
        "next": "https://api.wezafe.se/api/v1/guides?page=2",
        "self": "https://api.wezafe.se/api/v1/guides"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 7,
        "path": "https://api.wezafe.se/api/v1/guides",
        "per_page": 10,
        "to": 10,
        "total": 63
    }
}
```
Use this endpoint to get all guides/guides by ID
###HTTP Request:
`GET https://api.wezafe.se/api/v1/guides/{guide_id}`
###Query Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
id | int | no | 360005002431
#Alarms
##Get all user's alarms

```json
curl -X GET "https://api.wezafe.se/api/v1/user/alarms"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
```json
{
    "data": [
        {
            "id": 1,
            "text": "You've got bank accounts with negative balance in your bank report",
            "status": "NEW",
            "original_event_id": "1",
            "guide_id": "115004212793",
            "category": "financial",
            "date": "2018-10-05 10:53:23",
            "created_at": "2018-10-05 10:53:23"
        },
        {
            "id": 2,
            "text": "You've got transactions with recurring payments in your bank report",
            "status": "NEW",
            "original_event_id": "2",
            "guide_id": "115004212793",
            "category": "financial",
            "date": "2018-10-05 10:53:23",
            "created_at": "2018-10-05 10:53:23"
        }
    ],
    "links": {
        "first": "http://api.stage.wezafe.se/api/v1/user/alarms?page=1",
        "last": "http://api.stage.wezafe.se/api/v1/user/alarms?page=1",
        "prev": null,
        "next": null,
        "self": "http://api.stage.wezafe.se/api/v1/user/alarms"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http://api.stage.wezafe.se/api/v1/user/alarms",
        "per_page": 15,
        "to": 2,
        "total": 2
    }
}
```
Use this endpoint to get all user's alarms
###HTTP Request:
`GET https://api.wezafe.se/api/v1/user/alarms/`
##Get alarm by ID
```json
curl -X GET "https://api.wezafe.se/api/v1/user/alarms/{alarm_id}"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
```json
{
    "data": {
        "id": 2,
        "text": "You've got transactions with recurring payments in your bank report",
        "status": "NEW",
        "original_event_id": "2",
        "guide_id": "115004212793",
        "category": "financial",
        "date": "2018-10-05 10:53:23",
        "created_at": "2018-10-05 10:53:23"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/alarms/2"
    }
}
```
Use this endpoint to get alarm by id
###HTTP Request:
`GET https://api.wezafe.se/api/v1/user/alarms/{alarm_id}`
##Change alarm's status

```json
curl -X GET "https://api.wezafe.se/api/v1/user/alarms/2/close"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
```json
{
    "data": {
        "id": 2,
        "text": "You've got transactions with recurring payments in your bank report",
        "status": "CLOSED",
        "original_event_id": "2",
        "guide_id": "115004212793",
        "category": "financial",
        "date": "2018-10-05 10:53:23",
        "created_at": "2018-10-05 10:53:23"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/alarms/2/close"
    }
}
```
Use this endpoint to change status of alarm to "CLOSED"
###HTTP Request:
`GET https://api.wezafe.se/api/v1/user/alarms/{alarm_id}/close`
##Alarm's statistic
```json
curl -X GET "http://api.wezafe.se/api/v1/user/alarms/statistic?from_date=2018-11-08&to_date=2018-12-12"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
```json
{
    "data": {
        "alarms_for_days": [
            {
                "total": 1,
                "date": "2018-11-13"
            },
            {
                "total": 1,
                "date": "2018-11-16"
            }
        ],
        "alarms_for_categories": [
            {
                "total": 1,
                "category": "financial"
            },
            {
                "total": 1,
                "category": "identity"
            }
        ]
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/alarms/statistic"
    }
}
```
Use this endpoint to get statistic of alarms for user by filters
###HTTP Request:
`GET http://api.wezafe.se/api/v1/user/alarms/statistic?from_date=2018-11-08&to_date=2018-12-12`
###Query Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
from_date | str | no | 2018-11-08
from_date | str | no | 2018-12-12
category | str | no | identity
#Email
##Get email report
>Example request:

```json
curl -X GET "http://api.wezafe.se/api/v1/reports/hackad/aa@aa.com"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 200 OK

```json
{
    "data": {
        "isHacked": true,
        "reports": [
            {
                "website": "stratfor.com",
                "dateLeaked": "2011-11-30 23:00:00",
                "severity": 6,
                "eventId": 8939
            },
            {
                "website": "myspace.com",
                "dateLeaked": "2016-05-26 22:00:00",
                "severity": 6,
                "eventId": 8940
            },
            {
                "website": "funimation.com",
                "dateLeaked": "2016-06-30 22:00:00",
                "severity": 5,
                "eventId": 8958
            },
            {
                "website": "xsplit.com",
                "dateLeaked": "2013-11-06 23:00:00",
                "severity": 5,
                "eventId": 8959
            },
            {
                "website": "mangatraders.com",
                "dateLeaked": "2014-06-08 22:00:00",
                "severity": 5,
                "eventId": 8960
            }
        ]
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/reports/hackad/aa@aa.com"
    }
}
```
Use this endpoint to get hackad report for email
###HTTP Request:
`GET http://api.wezafe.se/api/v1/reports/hackad/{email}`


##Get user's watchlist
```json
curl -X GET "http://api.wezafe.se/api/v1/user/emails-watchlist"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
```json
{
    "data": [
        {
            "id": 2,
            "email": "georgebordiuh@gmail.com"
        }
    ],
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/emails-watchlist"
    }
}
```
Use this endpoint to get all user's watchlisted emails
###HTTP Request:
`GET http://api.wezafe.se/api/v1/user/emails-watchlist`
##Add email to watchlist
>Example request:

```json
curl -X POST "http://api.wezafe.se/api/v1/user/emails-watchlist"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"email":"test@gmail.com"}'
```
>Example response: 201 CREATED

```json
{
    "data": {
        "id": 98,
        "email": "test@gmail.com"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/emails-watchlist"
    }
}
```
Use this endpoint to add email to user's watchlist
###HTTP Request:
`GET http://api.wezafe.se/api/v1/user/emails-watchlist`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
email | str | no | test@gmail.com
##Delete email from watchlist
>Example request:

```json
curl -X DELETE "http://api.wezafe.se/api/v1/user/emails-watchlist/{email_id}"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 204 No content

Use this endpoint to add email to user's watchlist
###HTTP Request:
`DELETE http://api.wezafe.se/api/v1/user/emails-watchlist/{email_id}`
#Credit cards
##Get user's credit cards
>Example request:

```json
curl -X GET "http://api.stage.wezafe.se/api/v1/user/credit-cards"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "id": 95,
            "number": "214141******2412",
            "first_name": "Vitaliy",
            "last_name": "Asds",
            "expiration_date": "11/19",
            "created_at": "2018-11-16 10:55:02"
        },
        {
            "id": 96,
            "number": "242141******4124",
            "first_name": "Demo",
            "last_name": "Vitaliy",
            "expiration_date": "12/41",
            "created_at": "2018-11-22 08:13:35"
        }
    ],
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/credit-cards"
    }
}
```
Use thid endpoint to get all user's credit cards
###HTTP Request:
`GET http://api.stage.wezafe.se/api/v1/user/credit-cards`
##Add credit card to user's watchlist
>Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/user/credit-cards"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 201 CREATED

```json
{
    "data": {
        "id": 97,
        "number": "214121******2412",
        "first_name": null,
        "last_name": null,
        "expiration_date": null,
        "created_at": "2018-11-22 08:17:33"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/credit-cards"
    }
}
```
Use this endpoint to add credit card to user's watchlist
###HTTP Request:
`POST http://api.stage.wezafe.se/api/v1/user/credit-cards`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
number | str | yes | 214141******2412
first_name | str | no | Vitaliy
last_name | str | no | Tripuz
expiration_date | str | no | 11/19
##Delete credit card from user's watchlist
>Example request:

```json
curl -X DELETE "http://api.stage.wezafe.se/api/v1/user/credit-cards/97"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 204 No Content

Use this endpoint to remove credit card from monitoring
###HTTP Request:
`DELETE http://api.stage.wezafe.se/api/v1/user/credit-cards/{id}`
#Family
##Get invites
>Example request:

```json
curl -X GET "http://api.stage.wezafe.se/api/v1/user/family-members/invites"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "id": 13,
            "email": "lol@lol.ru",
            "first_name": "vitaliy",
            "last_name": "tripuz",
            "account_id": null,
            "status": "PENDING",
            "used_at": null
        }
    ],
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/family-members/invites"
    }
}
```
Use this endpoint to get all invites of family supervisor
###HTTP Request:
`GET http://api.stage.wezafe.se/api/v1/user/family-members/invites`
##Send invite to member
>Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/user/family-members/invites"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"email":"test@gmail.com"},{"first_name":"George"},{"last_name":"Bordiuh"}'
```
>Example response: 204 No content

Use this endpoint to create invite for new family member
###HTTP Request:
`POST http://api.stage.wezafe.se/api/v1/user/family-members/invites`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
email | str | yes | test@gmail.com
first_name | str | yes | Vitaliy
last_name | str | yes | Tripuz
##Resend invite
>Example request:

```json
curl -X GET "http://api.stage.wezafe.se/api/v1/user/family-members/invites/13"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 204 No content

Use this endpoint to resend email with invite to invites with status "PENDING"
###HTTP Request:
`GET http://api.stage.wezafe.se/api/v1/user/family-members/invites/{invite_id}`
##Delete invite
>Example request:

```json
curl -X DELETE "http://api.stage.wezafe.se/api/v1/user/family-members/invites/13"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 204 No content

Use this endpoint for deleting invites with status "PENDING"
###HTTP Request:
`GET http://api.stage.wezafe.se/api/v1/user/family-members/invites/{invite_id}`
##Delete family member
>Example request:

```json
curl -X DELETE "http://api.stage.wezafe.se/api/v1/user/family-members/76"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
```
>Example response: 204 No content

Use this endpoint to delete user from family. After this request user, who was deleted from family will be downgraded to free subscription
###HTTP Request:
`DELETE http://api.stage.wezafe.se/api/v1/user/family-members/{user_id}`
#Confirmation
##Send phone confirmation code

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/user/verify/phone/send-code"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"country_code":"+380"},{"phone":"+380679231795"}'
```
>Example response: 204 No content

Use this endpoint to get confirmation code for phone confirmation. After this request you will receive sms with confirmation code
###HTTP Request:
`POST http://api.stage.wezafe.se/api/v1/user/verify/phone/send-code`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
country_code | str | yes | country prefix of mobile phone number
phone | str | yes | +465282360440
##Verify phone confirmation code
>Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/user/verify/code/"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"code":"384933"}'
```
>Example response: 204 No content

 Use this endpoint to verify code from sms witch you was get from <a href='index.html?json#send-phone-confirmation-coden'>this request</a>
###HTTP Request:
`POST http://api.stage.wezafe.se/api/v1/user/verify/code/`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
country_code | str | yes | country prefix of mobile phone number
code | int | yes | 384933
#Payments
##Update payment info Stripe
>Example request:

```json
curl -X PATCH "http://api.stage.wezafe.se/api/v1/user/payments/payment-info"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"payment_provider":"1"},{"payment_token":"tok_1DZzjKEq9dlk7M0HUiOTM6Sh"}'
```
>Example response: 200 OK

```json
{
    "data": {
        "account": {
            "id": "72",
            "first_name": "Georgetri",
            "last_name": "Швеция",
            "email": "tripuz99@gmail.com",
            "ssn": "194510101019",
            "image": null,
            "country": "SE",
            "country_full": "Sweden",
            "city": "Kiev",
            "street": "Street",
            "zipcode": "33004",
            "payment_city": "Kiev",
            "payment_street": "Street",
            "payment_zipcode": "33004",
            "phone": "+380679231795",
            "type": "SINGLE",
            "parent_id": null,
            "has_used_trial": 0,
            "is_phone_confirmed": false,
            "is_email_confirmed": true,
            "is_ssn_confirmed": true,
            "is_password_set": true,
            "shield_status": "RED",
            "new_alarms_count": 24,
            "total_alarms_count": 27,
            "created_at": "2018-10-29 16:37:43",
            "updated_at": "2018-11-24 11:56:58"
        },
        "subscription": {
            "status": "ACTIVE",
            "start_date": "2018-10-29",
            "end_date": "2019-11-24",
            "cancellation_date": null,
            "next_payment_date": null,
            "trial_end_date": "2018-12-08",
            "plan": {
                "id": 4,
                "title": "WeZafe Premium Sweden with monthly payment",
                "short_title": "WeZafe Premium Single",
                "description": "Paid plan with all WeZafe features for a single user with monthly payment",
                "type": "SINGLE",
                "interval": "MONTHLY",
                "plan_type": "REGULAR",
                "total_plan_duration_in_month": 12,
                "currency": "SEK",
                "full_payment_amount": 118800,
                "country": "SE",
                "trial_duration_in_days": 14,
                "extended_description": "24/7 monitoring of your personal identity, your e-mail & social media accounts and your bank accounts and credit cards.<br>Alarms in real time via SMS and email in case of suspected theft of your personal data",
                "url": "user/payments/subscription/single",
                "is_cancellable": 0,
                "price_formatted": "99 SEK / month"
            },
            "payment_details": {
                "provider_name": "SweetPay",
                "provider_payment_method": "invoice",
                "payment_account": null,
                "available_payment_accounts": {
                    "has_stripe_account": true,
                    "has_arvato_account": false
                }
            }
        },
        "company": null,
        "personal_protection": {
            "credit_score": null,
            "credit_score_text": null,
            "active": false
        }
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/payments/payment-info"
    }
}
```
Use this endpoint to patch user's payment information. In response should be updated payment data.
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
payment_provider | int | yes | for Stripe id is "1"
payment_token | str | yes | this token user is receiving from stripe after success payment

###HTTP Request:
`PATCH http://api.stage.wezafe.se/api/v1/user/payments/payment-info`
##Update payment info for SweetPay
>Example request:

```json
curl -X POST "http://api.stage.wezafe.se/api/v1/user/payment-information"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + YOUR_ACCESS_TOKEN"
-d '{"city":"Stockholm"},{"zipcode":"12345"},{"street":"mekhanizatoriv str"}'
```
>Example response: 200 OK

```json
{
    "data": {
        "payment_zipcode": "12345",
        "payment_city": "Stockholm",
        "payment_street": "street",
        "ssn": "194510101019"
    }
}
```
Use this endpoint to change payment addres for users with SweetPay as a payment provider
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
city | int | yes | Stockholm
zipcode | str | yes | 12345
street | str | yes | mekhanizatoriv
###HTTP Request:
`POST "http://api.stage.wezafe.se/api/v1/user/payment-information `

##Get Billings history of user
> Example request:

```json
curl -X GET "https://api.wezafe.se/api/v1/user/payments/billings"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
```
>Example response: 200 OK

```json
{
    "data": [
        {
            "date": "2018-11-18",
            "description": "Credit card payment for subscription",
            "amount": "199.0",
            "currency": "SEK",
            "provider": "Stripe",
            "status": "PAID",
            "account_id": null
        },
        {
            "date": "2018-10-17",
            "description": "Credit card payment for subscription",
            "amount": "199.0",
            "currency": "SEK",
            "provider": "Stripe",
            "status": "PAID",
            "account_id": null
        }
    ],
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/user/payments/billings"
    }
}
```
Use this endpoint to get all payments for some user
###HTTP Request:
`GET https://api.wezafe.se/api/v1/user/payments/billings`
#Orders
##Create B2B Order

> Example request:

```json
curl -X GET "https://api.wezafe.se/api/v1/orders/b2b/create-and-process"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
-d '{"name":"Kolme lakimiestä Oy"},{"org_number":"0288219-0"},{"sub_domain":"asiakastieto15"},
{"zipcode":"12345"},{"supervisor_first_name":"Vito"},{"street":"test"},
{"supervisor_phone":"+380679231795"},{"supervisor_last_name":"Papuchi"},
{"city":"Kiev"},{"email":"tripuz15@mail.ru"},{"country_id":"73"},{"plan_id":"12"},
{"phone":"+380679231795"}',{"ip":"192.0.0.1"},{"starting_fee":"13"},
{"supervisor_email":"tripuz15@mail.ru"},{"is_confirmation_required":"0"}'
```
>Example response: 201 Created

```json
{
    "data": {
        "id": 7,
        "name": "Kolme lakimiestä Oy",
        "org_number": "0288219-0",
        "sub_domain": "asiakastieto15",
        "email": "tripuz15@mail.ru",
        "street": "test",
        "zipcode": "12345",
        "city": "Kiev",
        "phone": "+380679231795",
        "country_code": "FI",
        "status": "SUCCESS",
        "supervisor_first_name": "Vito",
        "supervisor_last_name": "Papuchi",
        "supervisor_phone": "+380679231795",
        "ip": "192.0.0.1",
        "comment": null,
        "subscription_id": 12,
        "order_failed_reason": null,
        "sales_agency": null,
        "sales_person": null,
        "created_at": "2018-12-06 14:14:53",
        "updated_at": "2018-12-06 14:15:00"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/orders/b2b/create-and-process"
    }
}
```
Use this endpoint to create and process B2B order
###HTTP Request:
`POST https://api.wezafe.se/api/v1/orders/b2b/create-and-process`
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
name | string | yes | Kolme lakimiestä Oy
org_number | string | yes | 0288219-0
sub_domain | string | yes | asiakastieto15
zipcode | int | yes | 12345
supervisor_first_name | string | yes | vitaliy
supervisor_last_name | string | yes | tripuz
street | string | yes | mekhanizatoriv
supervisor_phone | string | yes | +380679231795
city | string | yes | Kiev
country_id | int | yes | 73
plan_id | str | yes | 12
phone | string | yes | +380679231795
ip | string | yes | 192.0.0.1
starting_fee | int | yes | 13
supervisor_email | string | yes | tripuz15@mail.ru
is_confirmation_required | boolean | yes | 1
email | string | yes | tripuz15@mail.ru
##Create B2C order

> Example request:

```json
curl -X GET "https://api.wezafe.se/api/v1/orders/b2c/create-and-process"
-H "Accept: application/json"
-H "client-id: your client_id"
-H "client-secret: your client_secret"
-H "Authorization: Bearer + access_token from previous request"
-d '{"zipcode":"12345"},{"first_name":"Vito"},{"street":"test"},{"last_name":"Papuchi"},
{"city":"Kiev"},{"email":"tripuz15@mail.ru"},{"country_id":"73"},{"plan_id":"8"},{"ip":"192.0.0.1"}'
```

>example response: 201 Created

```json
{
    "data": {
        "id": 2,
        "email": "tripuz12@mail.ru",
        "ssn": null,
        "first_name": "Vito",
        "last_name": "Papuchi",
        "city": "Kiev",
        "street": "test",
        "zipcode": "12345",
        "country_code": "FI",
        "phone": null,
        "status": "SUCCESS",
        "ip": "192.0.0.1",
        "comment": null,
        "subscription_id": 13,
        "account_id": 13,
        "order_failed_reason": null,
        "plan": {
            "id": 8,
            "title": "WeZafe Premium Finland with monthly payment",
            "short_title": "WeZafe Premium Single",
            "description": "Paid plan with all WeZafe features for a single user with monthly payment",
            "type": "SINGLE",
            "interval": "MONTHLY",
            "plan_type": "REGULAR",
            "total_plan_duration_in_month": 12,
            "currency": "EUR",
            "full_payment_amount": 11880,
            "country": "FI",
            "trial_duration_in_days": 14,
            "extended_description": "24/7 monitoring of your personal identity, your e-mail & social media accounts and your bank accounts and credit cards.<br>Alarms in real time via SMS and email in case of suspected theft of your personal data",
            "url": "user/payments/subscription/single",
            "is_cancellable": 0,
            "price_formatted": "9.9 EUR / month"
        },
        "lead_id": null,
        "track_id": null,
        "utm_medium": null,
        "utm_source": null,
        "utm_campaign": null,
        "sales_agency": null,
        "sales_person": null,
        "sales_date": null,
        "created_at": "2018-12-06 14:56:30",
        "updated_at": "2018-12-06 14:56:36"
    },
    "links": {
        "self": "http://api.stage.wezafe.se/api/v1/orders/b2c/create-and-process"
    }
}
```
Use this endpoint to create and process B2C order
###Parameters:
Parameter | type | Required | Example
-------------- | -------------- | --------------
zipcode | int | yes | 12345
first_name | string | yes | vitaliy
last_name | string | yes | tripuz
street | string | yes | mekhanizatoriv
city | string | yes | Kiev
country_id | int | yes | 73
plan_id | str | yes | 12
ip | string | yes | 192.0.0.1
email | string | yes | tripuz15@mail.ru


<!-- END_INFO -->
